package controle;

import java.util.ArrayList;
import modelo.Pessoa;

public class AgendaControle {
    
    private ArrayList<Pessoa> listaPessoas;
    
    public AgendaControle(){   
        listaPessoas = new ArrayList<Pessoa>();  
    }
    
    public String adicionar(Pessoa umaPessoa){
        String mensagem = "Pessoa adicionada com sucesso!";
        listaPessoas.add(umaPessoa);
        return mensagem;
    }
    
    public void remover(Pessoa umaPessoa){
        listaPessoas.remove(umaPessoa);
    }
    
    public Pessoa buscar(String umNome){
        //for iterativo
        for(Pessoa umaPessoa: listaPessoas){
            String nomeAtual = umaPessoa.getNome();
            
            if(nomeAtual.equalsIgnoreCase(umNome)){
                return umaPessoa;
            }
            else{
                System.out.println("Pessoa não encontrada.");
            }

        }
        return null;
    }                                           
}
