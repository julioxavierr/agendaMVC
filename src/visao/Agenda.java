package visao;

import java.util.Scanner;
import modelo.Pessoa;
import controle.AgendaControle;

public class Agenda {

    private static AgendaControle minhaAgenda;

    public static void main(String[] args) {

        Scanner leitor = new Scanner(System.in);
        Scanner secondLeitor = new Scanner(System.in);
        minhaAgenda = new AgendaControle();
        int i = 1;
        
        while(i==1){
        System.out.println("Digite um número para escolher uma opção: ");
        System.out.println("1 - Cadastrar uma nova pessoa.");
        System.out.println("2 - Buscar uma pessoa.");
        System.out.println("3 - Sair do programa.");
        
        int option = leitor.nextInt();
        System.out.println("--------------------------------");
        
        if(option == 1){
            criarPessoa();
        }
        else if(option == 2){
            buscarPessoa();
        }
        else if(option == 3){
            break;
        }
        else{
            System.out.println("Você escolheu uma opção incorreta, digite 1 ou 2");
        }
        
        System.out.print("Se deseja continuar, aperte 1. Para abortar o programa, aperte 0: ");
        i = leitor.nextInt();
        System.out.println("--------------------------------");
        
        }

    }

    public static void criarPessoa() {
        Scanner leitor = new Scanner(System.in);
        Pessoa umaPessoa;
        System.out.println("Digite o nome: ");
        String umNome = leitor.nextLine();
        umaPessoa = new Pessoa(umNome);

        System.out.println("Digite um telefone: ");
        String umTelefone = leitor.nextLine();
        umaPessoa.setTelefone(umTelefone);

        System.out.println("Digite o email: ");
        String umEmail = leitor.nextLine();
        umaPessoa.setEmail(umEmail);

        // -----------
        System.out.println(minhaAgenda.adicionar(umaPessoa));
    }
    
    public static void buscarPessoa(){
        Scanner leitor = new Scanner(System.in);
        System.out.println("Digite o nome que deseja encontrar: ");
        String nome = leitor.nextLine();
        if(minhaAgenda.buscar(nome)!=null){
        System.out.println("Informações da pessoa: ");
        System.out.println(minhaAgenda.buscar(nome).getNome());
        System.out.println(minhaAgenda.buscar(nome).getTelefone());
        System.out.println(minhaAgenda.buscar(nome).getEmail());
        }
       
    }

}
